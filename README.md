# README #
This is a simple example of a SLA Driven API. You can run it by building the WAR file and deploying it into any Java webserver.

### Contact ###

* Academic issues: José Antonio Parejo - japarejo@us.es
* Technical issues: Antonio Gámez - agamez2@us.es