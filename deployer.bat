@ECHO ON
echo "HEROKU DEPLOYER"
::SET /p id=application-id:
::SET /p war=war:

SET id=showcase-papamoscas
SET war=target/showcase-papamoscas-1.0.war
heroku deploy:war --war %war% --app %id%
heroku ps:scale web=1 --app %id%
pause
exit