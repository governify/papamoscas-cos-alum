package es.us.isa.papamoscas.services;

import es.us.isa.papamoscas.entities.Bird;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Applied Software Engineering Research Group (ISA Group) University of
 * Sevilla, Spain
 *
 * @author Antonio Gamez <agamez2@us.es>
 * @version 1.0
 */
public class PersistenceMap {

    private static PersistenceMap instance;
    private static List<Object> agr;

    protected PersistenceMap() {
    }

    public static PersistenceMap getInstance() {
        if (instance == null) {
            init();
            instance = new PersistenceMap();
        }
        return instance;
    }

    public static List<Object> getPersistenceMap() {
        return agr;
    }

    public static void init() {
        agr = new ArrayList<Object>();
        Bird b = new Bird(new UUID(10, 10), "Papamoscas Gris", "Sierra de Buitrago", 21, 15, 31, 12);
        agr.add(b);
    }

    public void create(Object o) {
        agr.add(o);
    }

    void update(UUID id, Bird entity) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    void delete(UUID id) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    Bird select(UUID id) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    List<Bird> selectAll() {
        return new ArrayList(agr);
    }

    String count() {
        throw new UnsupportedOperationException("Not supported yet.");
    }
}
