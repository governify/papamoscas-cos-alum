package es.us.isa.papamoscas.services;

import es.us.isa.papamoscas.entities.AbstractFacade;
import es.us.isa.papamoscas.entities.Bird;
import java.util.List;
import java.util.Random;
import java.util.UUID;
import javax.persistence.EntityManager;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 * Applied Software Engineering Research Group (ISA Group) University of
 * Sevilla, Spain
 *
 * @author Antonio Gámez <agamez2@us.es>
 * @version 1.0
 */
@Path("birds")
public class BirdFacadeREST extends AbstractFacade<Bird> {

    PersistenceMap m = PersistenceMap.getInstance();

    public BirdFacadeREST() {
        super(Bird.class);
    }

    /* POST */
    @POST
    @Override
    @Consumes(MediaType.APPLICATION_JSON)
    public void create(Bird entity) {
        m.create(entity);
    }

    /* PUT */
    @PUT
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    public void edit(@PathParam("id") UUID id, Bird entity) {
        m.update(id, entity);
    }

    /* DELETE */
    @DELETE
    @Path("{id}")
    public void remove(@PathParam("id") UUID id) {
        m.delete(id);
    }

    /* GET */
    @GET
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Bird find(@PathParam("id") UUID id) {
        return m.select(id);
    }

    @GET
    @Override
    @Produces(MediaType.APPLICATION_JSON)
    public List<Bird> findAll() {
        return m.selectAll();
    }

    @GET
    @Path("count")
    @Produces(MediaType.TEXT_PLAIN)
    public String countREST() {
        return m.count();
    }

    /* Analytics */
    @GET
    @Path("analytics/slow")
    @Produces(MediaType.TEXT_PLAIN)
    public String slow() throws InterruptedException {
        Thread.sleep(2000);
        return "OK-slow: " + Math.abs(new Random().nextGaussian() * 5);
    }

    @GET
    @Path("analytics/fast")
    @Produces(MediaType.TEXT_PLAIN)
    public String fast() {
        return "OK-fast: " + Math.abs(new Random().nextGaussian() * 5);
    }

    @Override
    protected EntityManager getEntityManager() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

}
