package es.us.isa.papamoscas.filters;

import com.google.gson.Gson;

import java.io.BufferedReader;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import es.us.isa.papamoscas.entities.Property;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Applied Software Engineering Research Group (ISA Group) University of
 * Sevilla, Spain
 *
 * @author Antonio Gamez <agamez2@us.es>
 * @version 1.0
 */


public class AgreementFilter implements Filter {

    private static final Logger LOG = Logger.getLogger(AgreementFilter.class.getName());
    private String clientId;

    private final static String serverURL = "http://datastore.governify.io/api/v5/agreements";
    protected static Gson g = new Gson();

    protected String getClientId() {
        return clientId;
    }

    protected void setClientId(String clientId) {
        this.clientId = clientId;
    }

    @Override
    public void init(FilterConfig fConfig) throws ServletException {
        ServletContext context = fConfig.getServletContext();
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) request;
        HttpServletResponse resp = (HttpServletResponse) response;
        String uri = req.getRequestURI();
        Boolean noFilter = uri.contains("ico") || uri.contains("css") || uri.contains("png") || uri.contains("jpg") || uri.contains("html");

        String user = req.getParameter("user");
        LOG.log(Level.INFO, "URL received: {0}", req.getRequestURI());
        LOG.log(Level.INFO, "User param received: {0}", user);
        setClientId(req.getParameter("user"));
        if (authorizeRequest(req) || noFilter) {
            Long timestamp = System.currentTimeMillis();
            chain.doFilter(request, response);
            timestamp = System.currentTimeMillis() - timestamp;
            responseDone(timestamp);
            requestDone(req, resp);
            LOG.log(Level.INFO, "Request accepted");
        } else {
            resp.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
            resp.sendRedirect("/error.html");
            LOG.log(Level.INFO, "Request rejected");
        }
    }

    private void responseDone(Long timestamp) {
        String json2 = doGetInternal("/" + getClientId() + "/properties/Requests");
        Property p2 = g.fromJson(json2, Property.class);

        String json = doGetInternal("/" + getClientId() + "/properties/AVGResponseTime");
        Property p = g.fromJson(json, Property.class);

        if (!(json.equals("ERROR")) || (json2.equals("ERROR"))) {
            long l = p.getValue() != null ? Double.valueOf(p.getValue().toString()).longValue() : 0;
            Long avgT = l;
            long i = p2.getValue() != null ? Double.valueOf(p2.getValue().toString()).longValue() : 0;
            Long numReq = i;
            if (numReq != 0) {
                avgT = (avgT * (numReq - 1) + timestamp) / numReq;
            } else {
                avgT = new Long(1000);
            }
            p.setValue(avgT.toString());
            doPostInternal("/" + getClientId() + "/properties/AVGResponseTime", p);
        } else {
        }
    }

    @Override
    public void destroy() {

    }

    private boolean authorizeRequest(HttpServletRequest req) {
        Boolean isAuth = false;

        String json = doGetInternal("/" + getClientId() + "/guarantees/RequestTerm");
        g = new Gson();
        Boolean gua = g.fromJson(json, Boolean.class);

        if (!json.equals("ERROR")) {
            isAuth = gua;
        } else {
            return false;
        }

        if (isAuth) {
            String resourcePath = req.getRequestURI();
            String[] resources = resourcePath.split("/");
            if (resources.length == 5) {
                if (resources[4].equals("analyticsFT") || resources[4].equals("analyticsSL")) {
                    String json2 = doGetInternal("/" + getClientId() + "/guarantees/AnalyticsRequestTerm");
                    Boolean ret = g.fromJson(json2, Boolean.class);
                    if (!json2.equals("ERROR")) {
                        isAuth = ret;
                    } else {
                        return false;
                    }
                }
            }
        } else {
            return false;
        }
        return isAuth;
    }

    private void requestDone(HttpServletRequest req, HttpServletResponse resp) {
        String resourcePath = req.getRequestURI();
        String[] resources = resourcePath.split("/");
        if (resources.length == 5) {
            if (resources[4].equals("analyticsFT") || resources[4].equals("analyticsSL")) {
                String json = doGetInternal("/" + getClientId() + "/properties/AnalyticsRequests");
                Property p = g.fromJson(json, Property.class);
                if (!json.equals("ERROR") && ((HttpServletRequest) resp).getHeader("already-sent") == null) {
                    int i = p.getValue() != null ? Double.valueOf(p.getValue().toString()).intValue() : 0;
                    Integer numReq = i;
                    numReq++;
                    p.setValue(numReq.toString());
                    doPostInternal("/" + getClientId() + "/properties/AnalyticsRequests", p);
                } else {
                }
            }
        }

        String json = doGetInternal("/" + getClientId() + "/properties/Requests");
        g = new Gson();
        Property p = g.fromJson(json, Property.class);

//        if (!json.equals("ERROR") && ((HttpServletRequest) resp).getHeader("req-done") == null) {
        if (!json.equals("ERROR")) {
            int i = p.getValue() != null ? Double.valueOf(p.getValue().toString()).intValue() : 0;
            Integer numReq = i;
            numReq++;
            p.setValue(numReq.toString());
            doPostInternal("/" + getClientId() + "/properties/Requests", p);
        } else {
        }
        resp.setHeader("req-done", "y");
    }

    private void doPostInternal(String url, Property p) {
        String userAgent = "Mozilla/5.0 (X11; Linux x86_64; rv:26.0) Gecko/20100101 Firefox/26.0";
        try {
            URL api = new URL(serverURL + url);
            HttpURLConnection connection = (HttpURLConnection) api.openConnection();
            connection.setDoOutput(true);
            connection.setDoInput(true);
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setRequestProperty("Accept", "application/json");
            String respuesta1 = g.toJson(p);
            OutputStreamWriter wr = new OutputStreamWriter(connection.getOutputStream());
            wr.write(respuesta1);
            wr.flush();
            int HttpResult = connection.getResponseCode();
            if (HttpResult == HttpURLConnection.HTTP_OK) {
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private String doGetInternal(String url) {
        String a = "ERROR";
        try {
            URL api = new URL(serverURL + url);
            StringBuilder out = new StringBuilder();
            BufferedReader in = new BufferedReader(new InputStreamReader(api.openStream()));
            String inputLine;
            while ((inputLine = in.readLine()) != null) {
                out.append(inputLine);
            }
            a = out.toString();
            in.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return a;
    }
}
